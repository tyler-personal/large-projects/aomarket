package main;

import cats.implicits._
import entities._
import interactors.AuctionComparer
import interactors.ParseFunctions._
import main.implicits._

import scala.collection.mutable.{Map => MutableMap}
import scala.concurrent._
import scala.concurrent.duration._
import scala.io.StdIn.readLine
import scala.sys.process._

object Main extends App {
  case class State(offers: Auction.Map, requests: Auction.Map, zone: Zone)
  case class Mutable[T](var value: T)

  val zone: Mutable[Zone]             = Mutable(Zone.Market.FortSterling())
  val offersMap: Auction.MutableMap   = MutableMap()
  val requestsMap: Auction.MutableMap = MutableMap()

  val compareAuctions =
    AuctionComparer.compareAuctions(offersMap.toMap, requestsMap.toMap)(_, _)

  print("Password: ")
  val pw = System.console().readPassword().mkString

  def lines = (s"echo '$pw'" #| "sudo -kS ./albiondata-client -d --debug").lazyLines

  def processCommand: Future[Unit] = Future {
    println(s"""
      |${"-".repeat(15)}
      |1. Offer Count
      |2. Request Count
      |3. List Offers
      |4. List Requests
      |5. Show Comparisons [FortSterling Offers & Current Requests]
      |6. Save
    """.stripMargin.trim)

    val result = parseCommand(
      offersMap.toMap,
      requestsMap.toMap,
      Zone.Market.FortSterling(),
      zone.value,
      readLine().toIntOption
    )
    println(result)
  } >>= (_ => processCommand)

  val processLines = Future {
    import Event.AuctionEv._

    lines.mapSome(Event(_)).foreach {
      case GetOffers(auctions)            => offersMap.addAuctions(zone.value, auctions)
      case GetRequests(auctions)          => requestsMap.addAuctions(zone.value, auctions)
      case Event.Join(_playerID, newZone) => zone.value = newZone
      case _                              => ()
    }
  }

  Await.ready(processCommand, (365 * 200) days)
}
