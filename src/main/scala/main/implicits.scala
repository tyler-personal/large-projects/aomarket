package main

import cats._, cats.implicits._

import scala.collection._
import scala.concurrent.ExecutionContext
import scala.reflect.ClassTag

object implicits {
  implicit val executionContext: ExecutionContext = ExecutionContext.global

  implicit def sortedSetMonoid[A: Ordering]: Monoid[SortedSet[A]] =
    new Monoid[SortedSet[A]] {
      def empty: SortedSet[A] = SortedSet[A]()

      def combine(x: SortedSet[A], y: SortedSet[A]): SortedSet[A] = x ++ y
    }

  implicit def immutableSortedSetMonoid[A: Ordering]: Monoid[immutable.SortedSet[A]] =
    new Monoid[immutable.SortedSet[A]] {
      def empty: immutable.SortedSet[A] = immutable.SortedSet[A]()

      def combine(
          x: immutable.SortedSet[A],
          y: immutable.SortedSet[A]
      ): immutable.SortedSet[A] = x ++ y

    }

  implicit class FunctionOps[A, B](fn: A => B) {
    def <^>[F[_]: Functor](x: F[A]): F[B] = x.map(fn)
  }

  implicit class ListOps[A](xs: List[A]) {
    def doubleAll(implicit ev: A =:= Int) = xs.map(_ * 2)
  }

  implicit class FunctorOps[F[_]: Functor, A](functor: F[A]) {
    def foreach[B](fn: A => Unit): Unit = functor.map(fn)
  }

  implicit class NestedFunctorOpts[F[_]: Functor, FF[_]: Functor, A](functor: F[FF[A]]) {

    // QUESTION: Why can't I use evidence within `FunctorOps` to construct:
    // [FF[_]: Functor, B] => (B => Unit) => (implict ev: A =:+ FF[B]) => Unit
    // with the same level of type inference at the call site as the below?

    def mapInner[B](fn: A => B): F[FF[B]] = functor.map(_.map(fn(_)))

    def foreachInner(fn: A => Unit): Unit = mapInner(fn)

  }

  implicit class StringOps(xs: String) {

    def splitFirst(other: String): Option[(String, String)] =
      xs.split(other).toList match {
        case x :: y :: ys => Some(x, (List(y) |+| ys).mkString(other))
        case _            => None
      }

  }

  implicit class CastOps[A](x: A) {

    def is[B: ClassTag]: Boolean = x match {
      case v: B => true
      case _    => false
    }

    def as[B: ClassTag]: Option[B] = x match {
      case v: B => Some(v)
      case _    => None
    }

  }

  implicit class MapSomeOp[M[_]: Monad, A](xs: M[A]) {

    def mapSome[B](f: A => Option[B])(implicit monoid: Monoid[M[B]]): M[B] =
      xs.flatMap(f(_).map(_.pure[M]).orEmpty)

  }

  implicit class MapOps[K, V](xs: mutable.Map[K, V])(implicit monoid: Monoid[V]) {

    // NOTE: If this isn't working at callsite, make sure there's no
    // `import scala.collection._` (maybe with cats.implicits._/main.implicits._?)
    def updateOrAdd(k: K, v: V): Option[V] = xs.updateWith(k)(x =>
      Some(x match {
        case Some(vv) => vv |+| v
        case _        => v |+| monoid.empty
      })
    )

  }

}
