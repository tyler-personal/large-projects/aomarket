package entities
import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

object Auctions {
  def apply(data: String): Option[List[Auction.JSON]] = {
    val matcher = """"([A-Z])([A-Za-z]*":)""".r
    val formattedData = matcher.replaceAllIn(data, _ match {
      case matcher(head, tail) => s""""${head.toLowerCase()}$tail"""
    }).replace("}", "},").dropRight(2).appended(']')

    decode[List[Auction.JSON]](formattedData).toOption
  }
}