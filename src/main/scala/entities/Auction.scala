package entities

import scala.collection.SortedSet
import scala.collection.{immutable, mutable}
import main.implicits._
import cats.kernel.Eq

import cats.implicits._

object Auction {
  type ItemName = String

  type MutableMap = mutable.Map[Zone, immutable.Map[ItemName, SortedSet[Record]]]
  type Map        = immutable.Map[Zone, immutable.Map[ItemName, SortedSet[Record]]]

  implicit class AuctionMapOps(map: Auction.MutableMap) {

    def addAuctions(zone: Zone, auctions: List[Record]) =
      map.updateOrAdd(
        zone,
        auctions
          .groupBy(_.itemID)
          .view
          .mapValues(SortedSet[Record]() ++ _.toSet)
          .toMap
      )

  }

  sealed trait Type

  object Type {
    case class Offer() extends Type

    case class Request() extends Type
  }

  final case class Record(
      id: String,
      unitPrice: Long,
      totalPrice: Long,
      units: Int,
      itemID: String,
      auctionType: Auction.Type
  )

  object Record {
    implicit val eq: Eq[Record] = Eq.fromUniversalEquals

    implicit val ord: Ordering[Record] = Ordering.by(record =>
      if (record.auctionType.is[Auction.Type.Offer])
        record.unitPrice
      else
        -record.unitPrice
    )

    def from(o: JSON, auctionType: Type): Record = Record(
      o.referenceId,
      o.unitPriceSilver / 1000,
      o.totalPriceSilver / 1000,
      o.amount,
      o.itemTypeId,
      auctionType
    )

  }

  case class Comparison(
      itemID: String,
      offer: (Zone, Record),
      request: (Zone, Record),
      units: Int,
      totalDifference: Long
  )

  case class JSON(
      id: Long,
      unitPriceSilver: Long,
      totalPriceSilver: Long,
      amount: Int,
      tier: Int,
      isFinished: Boolean,
      auctionType: String,
      hasBuyerFetched: Boolean,
      hasSellerFetched: Boolean,
      sellerCharacterId: Option[String],
      sellerName: Option[String],
      buyerCharacterId: Option[String],
      buyerName: Option[String],
      itemTypeId: String,
      itemGroupTypeId: String,
      enchantmentLevel: Int,
      qualityLevel: Int,
      expires: String,
      referenceId: String
  )

}
