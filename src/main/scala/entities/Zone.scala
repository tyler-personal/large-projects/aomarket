package entities

sealed trait Zone
object Zone {
  def apply(zoneID: Int): Zone = zoneID match {
    case 7 => Market.Thetford()
    case 1002 => Market.Lymhurst()
    case 3003 => Market.CaerleonBlackMarket()
    case 3005 => Market.Caerleon()
    case 3008 => Market.Martlock()
    case 4002 => Market.FortSterling()
    case _    => Surface.Unknown()
  }

  sealed trait Surface extends Zone
  object Surface {
    case class Unknown() extends Surface
  }

  sealed trait Market extends Zone
  object Market {
    case class FortSterling() extends Market
    case class Lymhurst() extends Market
    case class CaerleonBlackMarket() extends Market
    case class Caerleon() extends Market
    case class Thetford() extends Market
    case class Martlock() extends Market
  }
}

