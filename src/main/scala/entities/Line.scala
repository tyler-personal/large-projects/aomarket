package entities

import main.implicits._

case class Line(timestamp: String, raw: String, event: Option[Event])
object Line {

  def apply(line: String): Line = {
    val namePattern     = "Updating player to (.*)".r
    val simplePattern   = "\\[([0-9-T:]+)\\] (.*)".r
    val eventPattern    = "\\[([0-9-T:]+)\\] [A-Za-z]+: \\[([0-9]+)\\][\\w]+ - map(.*)".r
    val keyValuePattern = """([0-9]+:\[[^\]]+\]?|\S+)""".r

    line.drop(13) match {
      case eventPattern(timestamp, id, mapStr) if id.toIntOption.isDefined =>
        val eventID = id.toInt
        val map     = keyValuePattern
          .findAllIn(mapStr.drop(1).dropRight(1))
          .map(_.splitFirst(":").flatMap { case (x, y) => x.toIntOption.map((_, y)) })
          .collect { case Some(x) => x }
          .toMap
        Line(timestamp, line, Event(map, eventID))
      case simplePattern(timestamp, rest) =>
        Line(
          timestamp,
          line,
          rest match {
            case namePattern(name) => Some(Event.Name(name))
            case _                 => None
          }
        )
      case _ => Line("", line, None)
    }
  }

}
