package entities

import cats._, cats.implicits._, cats.data._, cats.instances._
import main.implicits._

sealed trait Event
object Event {
  case class Name(playerName: String) extends Event
  case class Join(playerID: Int, zone: Zone) extends Event
  case class NewCharacter(playerID: Int, playerName: String) extends Event

  sealed trait Combat extends Event
  object Combat {
    case class HealthUpdate(defenderID: Int, attackerID: Int, spellValue: Int, spellID: Int)
      extends Event.Combat
    case class CastSpell(casterID: Int, defenderID: Int, spellID: Int)
      extends Event.Combat
    case class CastHit(defenderID: Int, attackerID: Int, spellID: Int)
      extends Event.Combat
    case class CastHits()
      extends Event.Combat
  }

  sealed trait AuctionEv extends Event
  object AuctionEv {
    case class GetOffers(auctions: List[Auction.Record]) extends Event.AuctionEv
    case class GetRequests(auctions: List[Auction.Record]) extends Event.AuctionEv
  }

  def apply(map: Map[Int, String], eventID: Int): Option[Event] = {
    def str(index: Int) = map.get(index)
    def num(index: Int) = map.get(index).flatMap(_.toIntOption)

    val result: Option[Event] = eventID match {
      case 2  => Join.curried <^> num(0) <*> num(8).map(Zone(_))
      case 6  => Combat.HealthUpdate.curried <^> num(0) <*> num(6) <*> num(2) <*> num(7)
      case 18 => Combat.CastSpell.curried <^> num(0) <*> num(1) <*> num(3)
      case 19 => Combat.CastHit.curried <^> num(0) <*> num(1) <*> num(2)
      case 20 => Combat.CastHits().pure[Option]
      case 25 => NewCharacter.curried <^> num(0) <*> str(1)
      case 78 => AuctionEv.GetOffers <^>
        str(0).flatMap(Auctions(_)).map(_.map(Auction.Record.from(_, Auction.Type.Offer())))
      case 79 => AuctionEv.GetRequests <^>
        str(0).flatMap(Auctions(_)).map(_.map(Auction.Record.from(_, Auction.Type.Request())))
      case _  => None
    }
    result
  }

  def apply(line: String): Option[Event] = Line(line).event
}