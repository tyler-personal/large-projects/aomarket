package interactors

import cats.implicits._

import scala.collection.SortedSet

import main.implicits._
import entities._
import interactors.AuctionComparer
import main.Main.Mutable

object ParseFunctions {

  def parseCommand(
      offers: Auction.Map,
      requests: Auction.Map,
      offerZone: Zone,
      requestZone: Zone,
      option: Option[Int]
  ): String = {
    lazy val offer   = offers.get(offerZone)
    lazy val request = requests.get(requestZone)

    val compareAuctions: (Zone, Zone) => List[Auction.Comparison] =
      AuctionComparer.compareAuctions(offers, requests)(_, _)

    def display(kv: (Auction.ItemName, SortedSet[Auction.Record])): String =
      (kv._1 :: kv._2.map(x => s"\t$x").toList).mkString("\n")

    val result: Option[String] = option match {
      case Some(1) => offer.map(_.size.toString)
      case Some(2) => request.map(_.size.toString)
      case Some(3) => offer.map(_.toList.map(display(_)).mkString("\n\n"))
      case Some(4) => request.map(_.toList.map(display(_)).mkString("\n\n"))
      case Some(5) =>
        compareAuctions(offerZone, requestZone)
          .filter(_.totalDifference > 0)
          .sortBy(_.totalDifference)
          .mkString("\n\n")
          .pure[Option]
      case _ => None
    }
    result.getOrElse("")
  }

  def parseLine(
      offers: Auction.MutableMap,
      requests: Auction.MutableMap,
      zone: Mutable[Zone],
      line: String
  ): Unit = {
    import Event.AuctionEv._

    List(line).mapSome(Event(_)).foreach {
      case GetOffers(auctions)            => offers.addAuctions(zone.value, auctions)
      case GetRequests(auctions)          => requests.addAuctions(zone.value, auctions)
      case Event.Join(_playerID, newZone) => zone.value = newZone
      case _                              => ()
    }
  }

}
