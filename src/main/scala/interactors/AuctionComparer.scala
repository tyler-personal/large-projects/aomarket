package interactors

import entities.Auction
import entities.Zone

object AuctionComparer {

  def compareAuctions(
      offersMap: Auction.Map,
      requestsMap: Auction.Map
  )(offerZone: Zone, requestZone: Zone): List[Auction.Comparison] = (for {
    offersLocalMap   <- offersMap.get(offerZone)
    requestsLocalMap <- requestsMap.get(requestZone)
    comparisons = offersLocalMap.keys.toList
      .intersect(requestsLocalMap.keys.toList)
      .map { name =>
        val offers   = offersLocalMap(name).toList
        val requests = requestsLocalMap(name).toList

        var offerIndex   = -1
        var requestIndex = -1

        var offerUnits   = 0
        var requestUnits = 0

        var units           = 0
        var totalDifference = 0L

        def offersRemaining =
          if (offerUnits == 0) offerIndex + 1 < offers.size
          else offerIndex < offers.size

        def requestsRemaining =
          if (requestUnits == 0) requestIndex + 1 < requests.size
          else requestIndex < requests.size

        var done = false
        while (!done && offersRemaining && requestsRemaining) {
          if (offerUnits == 0) {
            offerIndex += 1
            offerUnits = offers(offerIndex).units
          }
          val offer = offers(offerIndex)

          if (requestUnits == 0) {
            requestIndex += 1
            requestUnits = requests(requestIndex).units
          }
          val request = requests(requestIndex)

          if (request.unitPrice < offer.unitPrice) { done = true }
          else {
            val unitCount = offerUnits.min(requestUnits)
            units        += unitCount
            offerUnits   -= unitCount
            requestUnits -= unitCount

            totalDifference += ((request.unitPrice * unitCount) - (offer.unitPrice * unitCount))
          }
        }

        Auction.Comparison(
          offers.head.itemID,
          (offerZone, offers.head),
          (requestZone, requests.head),
          units,
          totalDifference
        )
      }
  } yield comparisons).getOrElse(List())

}
