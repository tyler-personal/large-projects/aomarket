package lib

import scala.util.Random

object TestImplicits {

  implicit class MyIntOps(x: Int) {
    def thousand: Long    = x * 1_000L
    def million: Long     = x * 1_000_000L
    def billion: Long     = x.toLong * 1_000_000_000L
    def trillion: Long    = x.toLong * 1_000_000_000_000L
    def quadrillion: Long = x.toLong * 1_000_000_000_000_000L

    def times(f: Int => Unit) = (1 to x).foreach(f)
  }

  implicit class MyIterableOps[A](xs: Iterable[A]) {
    def random  = xs.toList(Random.nextInt(xs.size))
    def shuffle = Random.shuffle(xs.toList)
  }

}
