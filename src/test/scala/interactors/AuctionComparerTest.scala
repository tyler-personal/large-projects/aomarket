// import cats.implicits._
import entities.Auction
import entities.Auction.Type._
import entities.Zone.Market._
import interactors.AuctionComparer
import org.scalatest._
import lib.TestImplicits._

import scala.collection._
import scala.util.Random
import scala.util.chaining._

import org.scalatest.featurespec.AnyFeatureSpec

//import flatspec._
import matchers._

class AuctionComparerTest extends AnyFeatureSpec with should.Matchers with GivenWhenThen {

  def randomLymhurst(
      fortOffers: List[Auction.Record],
      increase: Int
  ): (Auction.Record, Auction.Record) =
    fortOffers.random.pipe(a =>
      (a, mkAuction(a.unitPrice + increase, a.itemID, Request()))
    )

  def calculateDifferences(
      fortOffers: List[Auction.Record]
  ): (List[Int], List[Auction.Record]) =
    List
      .fill(50)((50 to 1000).random.pipe { increase =>
        val (original, auction) = randomLymhurst(fortOffers, increase)
        val totalDiff           = increase * Math.min(original.units, auction.units)
        (totalDiff, auction)
      })
      .unzip

  Scenario("A simple comparison is made between FortSterling & Lymhurst") {
    100.times { n =>
      Given(s"$n: 50 LymhurstRequests > 50 FortOffers")
      val fortOffers                 = mkAuctions(50, Offer())
      val (differences, lymRequests) = calculateDifferences(fortOffers)

      val fortMap: Auction.MutableMap = mutable.Map()
      val lymMap: Auction.MutableMap  = mutable.Map()

      fortMap.addAuctions(FortSterling(), fortOffers)
      lymMap.addAuctions(Lymhurst(), lymRequests)

      When("A comparison is made between the zones' auctions")
      val comparison = AuctionComparer.compareAuctions(
        fortMap.toMap,
        lymMap.toMap
      )(FortSterling(), Lymhurst())

      Then("There should be exactly `differences` totalDifference")
      assert(comparison.map(_.totalDifference).sum === differences.sum)
    }
  }
  Scenario("A comparison is made between auctions in FortSterling & Lymhurst") {
    100.times { n =>
      Given(s"$n: 200 Fort, 100 Lymhurst, 50 LymhurstRequests > FortOffers")
      val fortOffers   = mkAuctions(100, Offer())
      val fortRequests = mkAuctions(100, Request())

      val (differences, lymhurst) = calculateDifferences(fortOffers)
      val moreLymhurst            = mkAuctions(50)

      val fortSterlingAuctions = (fortOffers ++ fortRequests).shuffle
      val lymhurstAuctions     = (lymhurst ++ moreLymhurst).shuffle

      val fortSterlingMap: Auction.MutableMap = mutable.Map()
      val lymhurstMap: Auction.MutableMap     = mutable.Map()

      fortSterlingMap.addAuctions(FortSterling(), fortSterlingAuctions)
      lymhurstMap.addAuctions(Lymhurst(), lymhurstAuctions)

      When("A comparison is made between the zones' auctions")
      val comparison = AuctionComparer.compareAuctions(
        fortSterlingMap.toMap,
        lymhurstMap.toMap
      )(FortSterling(), Lymhurst())

      Then("there should be at least 50 comparisons")
      assert(comparison.length >= 50)
      assert(comparison.map(_.totalDifference).sum > differences.sum)
    }
  }

  private def mkAuctions(
      n: Int,
      auctionType: Auction.Type = List(Offer(), Request()).random
  ): List[Auction.Record] =
    List.fill(n)(mkAuction(auctionType = auctionType))

  private def mkAuction(
      unitPrice: Long = Random.between(1, 10.thousand),
      itemID: String =
        s"T${(1 to 80).random}_${List("HIDE", "CLOTH", "ORE", "WOOD", "STONE").random}",
      auctionType: Auction.Type
  ) = {
    val id    = Random.between(1, 100.billion)
    val units = Random.between(1, 50)
    Auction.Record(
      id.toString,
      unitPrice,
      (unitPrice * units),
      units,
      itemID,
      auctionType
    )
  }

}
