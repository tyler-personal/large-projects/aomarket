val v3 = false

val flags = Seq(
    "-encoding",
    "UTF-8",
    "-feature",
    "-language:implicitConversions",
    // "-Xfatal-warnings" disabled during the migration,
)
val flags2 = Seq(
    "-deprecation",
//    "-Xfatal-warnings",
    "-language:higherKinds",
    "-language:existentials",
    "-language:postfixOps",
    "-Wunused:imports,privates,locals",
    "-Wvalue-discard"
)
val flags3 = Seq(
    "-language:strictEquality",
    "-unchecked",
    "-Yexplicit-nulls",
    "-Ycheck-all-patmat",
    // "-Xfatal-warnings"

    // "-Ysafe-init"
    "-source:3.0-migration"
)

def depsCirce(version: String) =
    Seq("core", "generic", "parser").map(lib => "io.circe" %% s"circe-$lib" % version)

def depsCats(core: String, effect: String) =
  Seq(("core", core), ("effect", effect))
    .map { case (lib, version) => "org.typelevel" %% s"cats-$lib" % version }

val deps = Seq(
    "com.novocode" % "junit-interface" % "0.11" % "test",
    "com.lihaoyi" % "ammonite_2.13.5" % "2.3.8-65-0f0d597f",

    // "com.lihaoyi" %% "utest" % "0.7.2" % "test"
)
val deps2 = Seq(
  "com.olegpy" %% "better-monadic-for" % "0.3.1", // .cross(CrossVersion.for3Use2_13)
  "io.github.timwspence" %% "cats-stm" % "0.10.1",
  "org.scalactic" %% "scalactic" % "3.2.7",
  "org.scalacheck" %% "scalacheck" % "1.14.1" % "test",
  "org.scalatest" %% "scalatest" % "3.2.7" % "test"
  ) ++ depsCats(core = "2.5.0", effect = "3.0.2") ++ depsCirce("0.14.0-M4")

val deps3 = Seq(
) ++ depsCats(core = "2.6.0", effect = "3.1.0") ++ depsCirce("0.14.0-M5")


lazy val root = project
    .in(file("."))
    .settings(
        name := "scala3-simple",
        version := "0.1.0",
        autoCompilerPlugins := true,

        scalaVersion := { if (v3) "3.0.0-RC2" else "2.13.5" },
        // crossScalaVersions ++= Seq("2.13.5", "3.0.0-RC1"),
        // testFrameworks += new TestFramework("utest.runner.Framework"),
        
        scalacOptions ++= (flags ++ { if (v3) flags3 else flags2 }),
        libraryDependencies ++= (deps ++ { if (v3) deps3 else deps2 }),
    )

scalacOptions in (Compile, console) ~= { _.filterNot(Set("-Ywarn-unused-import", "-Ywarn-unused:imports")) }
